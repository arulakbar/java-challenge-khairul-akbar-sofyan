package ist.challenge.khairulakbarsofyan.controller;

import ist.challenge.khairulakbarsofyan.dto.LoginDto;
import ist.challenge.khairulakbarsofyan.dto.RegisterDto;
import ist.challenge.khairulakbarsofyan.payload.ApiResponse;
import ist.challenge.khairulakbarsofyan.service.RegistrationService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("auth/")
@AllArgsConstructor
public class AuthController {

    private final RegistrationService registration;
    private final AuthenticationManager authenticationManager;

    @PostMapping("registrasi")
    public ResponseEntity<ApiResponse> registration(@Valid @RequestBody RegisterDto registerDto){
        if (registration.existsByUsername(registerDto.getUsername())){
            return new ResponseEntity<>(new ApiResponse(false,"Username sudah terpakai")
                    , HttpStatus.CONFLICT);
        }
        registration.registration(registerDto);
        return new ResponseEntity(new ApiResponse(true,"registrasi sukses"),HttpStatus.CREATED);
    }

    @PostMapping("login")
    public ResponseEntity<ApiResponse> login(@RequestBody LoginDto loginDto){

        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            loginDto.getUsername(),
                            loginDto.getPassword()
                    )
            );
        } catch (BadCredentialsException ex){
            throw new BadCredentialsException("Username dan / atau password kosong");
        }
        return new ResponseEntity(new ApiResponse(true,"Sukses Login"),HttpStatus.OK);
    }
}
