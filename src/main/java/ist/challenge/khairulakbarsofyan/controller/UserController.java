package ist.challenge.khairulakbarsofyan.controller;

import ist.challenge.khairulakbarsofyan.model.User;
import ist.challenge.khairulakbarsofyan.payload.ApiResponse;
import ist.challenge.khairulakbarsofyan.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/users")
@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping()
    public List<User> getUsers(){
        return userRepository.findAll();
    }

    @PutMapping("/{id}")
    public ResponseEntity<ApiResponse> editUser(@PathVariable Long id, @RequestBody User user){

        if (userRepository.existsByUsername(user.getUsername())){
            return new ResponseEntity<>(new ApiResponse(false,"Username sudah terpakai")
                    ,HttpStatus.CONFLICT);
        }

        User user1 = userRepository.findUserByUsername(user.getUsername())
                .orElseThrow();
        if (user.getPassword().equals(user1.getPassword())){
            return new ResponseEntity<>(new ApiResponse(false,"Password tidak boleh sama"),
                    HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(new ApiResponse(true,"Sukses"),HttpStatus.CREATED);
    }
}
