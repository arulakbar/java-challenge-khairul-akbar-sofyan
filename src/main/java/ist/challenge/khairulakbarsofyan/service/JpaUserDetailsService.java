package ist.challenge.khairulakbarsofyan.service;

import ist.challenge.khairulakbarsofyan.model.CustomUserDetails;
import ist.challenge.khairulakbarsofyan.model.User;
import ist.challenge.khairulakbarsofyan.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.function.Supplier;

@Service
public class JpaUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public CustomUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Supplier<UsernameNotFoundException> exceptionSupplier =
                () -> new UsernameNotFoundException("username tidak ditemukan");

        User user = userRepository.findUserByUsername(username)
                        .orElseThrow(exceptionSupplier);

         return new CustomUserDetails(user);
    }
}