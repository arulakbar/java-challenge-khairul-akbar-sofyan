package ist.challenge.khairulakbarsofyan.service;

import ist.challenge.khairulakbarsofyan.dto.RegisterDto;
import ist.challenge.khairulakbarsofyan.model.User;
import ist.challenge.khairulakbarsofyan.repository.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class RegistrationService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public RegistrationService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Transactional
    public void registration(RegisterDto registerDto){
        User user = new User(
                registerDto.getUsername(),
                passwordEncoder.encode(registerDto.getPassword())
        );
        userRepository.save(user);
    }

    public boolean existsByUsername(String username){
        return userRepository.existsByUsername(username);
    }
}
