package ist.challenge.khairulakbarsofyan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KhairulAkbarSofyanApplication {

	public static void main(String[] args) {
		SpringApplication.run(KhairulAkbarSofyanApplication.class, args);
	}

}
