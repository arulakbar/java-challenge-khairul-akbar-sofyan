package ist.challenge.khairulakbarsofyan;


import ist.challenge.khairulakbarsofyan.controller.AuthController;
import ist.challenge.khairulakbarsofyan.security.SecurityConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
//@ContextConfiguration(classes = SecurityConfig.class)
//@ExtendWith(SpringExtension.class)
//@WebMvcTest(controllers = {AuthController.class})
//@Import({SecurityConfig.class})
public class RegistrasiTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void registrationCreated() throws Exception {
        mockMvc.perform(post("/auth/registrasi").with(user("user"))
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"username\":\"ujang\", \"password\":\"12345\"}")
        ).andExpect(status().isCreated());
    }
}
